c3sets is an attempt to collect the various sets played at c3 events. it is built using hugo.

## contributing

if you want to contribute, either send me your links and i'll add them here as todo (and add the content as soon as i find time) or clone the repository, add the content yourself and send me a patch or merge request.

please use the following template for new entries:
```
---
title: "[<event>] <artist> - <title/description>"
date: <date published>
artists: ["<artist1>","<artist2>", ..]
events: ["<event1>", "<event2>", ..]
draft: false
---

<link to set>

```

and save it as `content/set/<event>-<artist>-<title/description>.md`.

or - if you have hugo installed - just type `hugo new set/<event>-<artist>-<title/description>.md`. This will add a new pre-filled file you can edit.


## resources

already included:
* https://archive.org/details/CouchsofaLiveSets
* https://www.c-radar.de/2021/01/rc3-lounge-resources/
* https://entropia.de/GPN18:Musik
* https://entropia.de/GPN19:Musik
* https://www.mixcloud.com/matthiasdamasty/
* https://www.mixcloud.com/LoungeControl/
* https://soundcloud.com/das-kraftfuttermischwerk/anti-error-at-32c3
* https://soundcloud.com/tasmo/32c3-antierror-lounge
* https://www.youtube.com/watch?v=lcnTSOICAPk
* https://www.mixcloud.com/shroombab/
* https://soundcloud.com/das-kraftfuttermischwerk


partial included (only event sets):
* https://hearthis.at/barbnerdy/
* https://soundcloud.com/barbnerdy/tracks
* https://www.mixcloud.com/b4m/
* https://www.mixcloud.com/tasmo/
* https://mixcloud.com/vidister/
* https://soundcloud.com/beh2342/


todo:
* https://git.elektrollart.org/Elektroll/ChaosMusic/src/branch/master/playlist
* https://twitter.com/c3lounge
* https://soundcloud.com/sh1bumi/
* https://www.mixcloud.com/BarbNerdy2/
* https://hearthis.at/tasmo
* https://soundcloud.com/dj-spock-ffm
* https://www.mixcloud.com/dj_spock/
